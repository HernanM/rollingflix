import React, { useState, useEffect } from "react";
import Card from "./Card";

const AntiOscarContainer = () => {
  const [movies, setMovies] = useState([]);

  useEffect(() => {
    const prueba = [
      {
        img:
          "https://occ-0-2100-185.1.nflxso.net/dnm/api/v6/X194eJsgWBDE2aQbaNdmCXGUP-Y/AAAABZItBUiLTjat8cMxVpD8FVwdHw8L31W8Q7tRcSNKmU8x-VWCez4Ijp6JOsq_QRDdzRfktUBsHcMU0PRC9tHDvEsApLzugl6SkcUQUEpgz-XaeKNVNBlmk-ROeFYg.jpg?r=d3e",
        title: "Archer",
        year: 2010,
      },
      {
        img:
          "https://occ-0-2100-185.1.nflxso.net/dnm/api/v6/X194eJsgWBDE2aQbaNdmCXGUP-Y/AAAABZItBUiLTjat8cMxVpD8FVwdHw8L31W8Q7tRcSNKmU8x-VWCez4Ijp6JOsq_QRDdzRfktUBsHcMU0PRC9tHDvEsApLzugl6SkcUQUEpgz-XaeKNVNBlmk-ROeFYg.jpg?r=d3e",
        title: "Archer",
        year: 2010,
      },
      {
        img:
          "https://occ-0-2100-185.1.nflxso.net/dnm/api/v6/X194eJsgWBDE2aQbaNdmCXGUP-Y/AAAABZItBUiLTjat8cMxVpD8FVwdHw8L31W8Q7tRcSNKmU8x-VWCez4Ijp6JOsq_QRDdzRfktUBsHcMU0PRC9tHDvEsApLzugl6SkcUQUEpgz-XaeKNVNBlmk-ROeFYg.jpg?r=d3e",
        title: "Archer",
        year: 2010,
      },
      {
        img:
          "https://occ-0-2100-185.1.nflxso.net/dnm/api/v6/X194eJsgWBDE2aQbaNdmCXGUP-Y/AAAABZItBUiLTjat8cMxVpD8FVwdHw8L31W8Q7tRcSNKmU8x-VWCez4Ijp6JOsq_QRDdzRfktUBsHcMU0PRC9tHDvEsApLzugl6SkcUQUEpgz-XaeKNVNBlmk-ROeFYg.jpg?r=d3e",
        title: "Archer",
        year: 2010,
      },
      
    ];
    setMovies(prueba);
  }, []);

  return (
    <div className="mb-10 flex flex-col">
      <h2 className="my-4 text-white text-lg ">Lo peor de lo peor</h2>
      <div className="flex flex-row">
        {movies.map((movie) => (
          <Card info={movie} />
        ))}
      </div>
    </div>
  );
};

export default AntiOscarContainer;
