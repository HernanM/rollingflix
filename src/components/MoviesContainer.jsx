import React, { useState, useEffect } from "react";
import Card from "./Card";

const MoviesContainer = ({movies}) => {
    console.log("movies::::::",movies)
    return (
    <div className="mb-10 flex flex-col">
      <h2 className="my-4 text-white text-lg ">Genero </h2>
      <div className="flex flex-row">
        {movies.map((movie) => (
          <Card info={movie} />
        ))}
      </div>
    </div>
  );
};

export default MoviesContainer;