import React from "react";

const Presentation = () => {
  return (
    <div className="text-white py-10">
      <h1 className="text-5xl font-semibold">Originales de Rollingflix</h1>
      <p className="w-2/4 text-lg font-normal">
        En Rollingflix encontrarás títulos originales increíbles que no están en
        ninguna otra parte. Películas, programas de TV, especiales y más
        pensados exclusivamente para ti.
      </p>
    </div>
  );
};
export default Presentation;
