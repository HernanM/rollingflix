import React from "react";

const Navbar = () => {
  return (
    <div className="flex flex-row items-center justify-between bg-black w-full h-16">
       <h1 className="ml-10 text-red-700 font-extrabold text-4xl">ROLLINGFLIX</h1>
      <div className="mr-10 flex flex-row items-center">
        <h2 className="mx-1 text-white text-sm font-thin" >PELÍCULAS Y PROGRAMAS ILIMITADOS</h2>
        <button className="mx-1 bg-red-600 hover:bg-red-500 text-white text-sm font-thin py-1 px-6 border border-red-700 rounded">
          UNIRTE AHORA
        </button>
        <button className="mx-1 bg-transparent hover:bg-gray-800 text-white text-sm font-thin hover:text-white py-1 px-6 border border-white hover:border-white rounded">
          INICIAR SESION
        </button>
      </div>
    </div>
  );
};

export default Navbar;
