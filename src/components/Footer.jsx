import React from "react";
const Footer = () => {
  return (
    <div className="flex flex-col text-gray-700 mt-5 pb-12">
      <h1 className=" text-lg mb-5">¿Preguntas? Contáctenos.</h1>
      <div className="flex flex-row text font-thin text-sm">
        <ul className="mr-16">
          <li className="mb-3">Preguntas Frecuentes</li>
          <li className="mb-3">Relaciones con Inversionistas</li>
          <li className="mb-3">Privacidad</li>
          <li className="mb-3">Prueba de Velocidad</li>
        </ul>
        <ul className="mr-16">
          <li className="mb-3">Centro de Ayuda</li>
          <li className="mb-3">Empleo</li>
          <li className="mb-3">Preferencias de Cookies</li>
          <li className="mb-3">Avisos Legales</li>
        </ul>
        <ul className="mr-16">
          <li className="mb-3">Cuenta</li>
          <li className="mb-3">Formas de Ver</li>
          <li className="mb-3">Información Corporativa</li>
          <li className="mb-3">Originales de Rollingflix</li>
        </ul>
        <ul className="mr-16">
          <li className="mb-3">Prensa</li>
          <li className="mb-3">Términos de Uso</li>
          <li className="mb-3">Contáctanos</li>
        </ul>
      </div>
    </div>
  );
};
export default Footer;
