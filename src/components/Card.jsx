import React from "react";

const Card = ({ info }) => {
  const { poster, title, year } = info;

  return (
    <div className="mx-2 flex flex-col">
      <img src={poster} style={{ width: "300px", height: "168px" }} />
      <div className="flex flex-row justify-between">
        <h2 className="text-white ">{title}</h2>
        <p className="text-white">{year}</p>
      </div>
    </div>
  );
};

export default Card;
