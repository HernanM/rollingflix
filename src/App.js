import React from "react";
import Navbar from "./components/Navbar";
import Presentation from "./components/Presentation";
import ContentContainer from "./components/ContentContainer";
import Footer from "./components/Footer"

function App() {
  return (
    <div>
      <Navbar />
      <div style={{ background: "#181818" }} className="px-10">
        <Presentation />
        <ContentContainer />
        <Footer/>
      </div>
    </div>
  );
}

export default App;
